During the workshop, we are only working with merged sources on the posit Cloud project, not locally.
Please see the corresponding directory at https://posit.cloud/content/7313968 for mock coded data.