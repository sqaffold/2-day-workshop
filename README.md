## SQAFFOLD and ROCK 2-day workshop

# Simple Qualitative Administration For File Organization, Licensing, and Development (SQAFFOLD)

SQAFFOLD was created to help the administration and organization of qualitative projects, as well as aid researchers in adhering to Open Science principles via making as much of the research process transparent as possible. SQAFFOLD is a collection of subdirectories and files. Most subdirectories contain:

**readMe--** files that tell you what the subdirectory contains and/or has tips and tools to help your research <br>
**SQAFFOLD--** files that are empty (or only contain a line of description), which are ready to be filled with your content <br>

Of course, both of these types of objects can be deleted or moved...As with anything in the SQAFFOLD directory. Feel free to rearrange it according to what makes sense for you and your project. SQAFFOLD really is only meant to *scaffold* your work.

# Reproducible Open Coding Kit (ROCK)

The ROCK is a standard for working with qualitative data implemented in an R package {rock}. It helps researchers organize data sources, designate attributes to the providers of the data, code and segment narratives, as well as perform various analyses. The "scripts" directory contains an .rmd file that can be employed to run {rock} commands; this script does not exhaust all of {rock} functionality. For more information on the ROCK, see https://rock.science.

The rendered version of the main script is hosted by GitLab Pages at https://sqaffold.gitlab.io/2-day-workshop.

# Citation and licensing
SQAFFOLD and the two-day workshop are licensed under CC0 1.0 Universal (public domain), which means you can employ them and modify them without having to cite the author. If you do wish to cite SQAFFOLD, you can do so as such:

Zörgő, S. (2023, August 9). SQAFFOLD. Retrieved from osf.io/zerwu <br>
For other formats, see the citation bar of the OSF project: https://osf.io/zerwu

The Reproducible Open Coding Kit (ROCK) standard is licensed under CC0 1.0 Universal.
The {rock} R package is licensed under a GNU General Public License; for more see: https://rock.science.

ROCK citation:
Gjalt-Jorn Ygram Peters and Szilvia Zörgő (2023). rock: Reproducible Open Coding Kit. R package version 0.7.1. https://rock.opens.science

For more on ROCK materials licensing and citation, please see: https://rock.opens.science/authors.html#citation.
<br>
<br>
<br>